using System.Text;
using Microsoft.EntityFrameworkCore;
using NotyService.Data;
using NotyService.Data.Entity;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Sender.Rabbit;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;
using System.Text.Json;

namespace Sender;

public class Worker : BackgroundService
{
    private readonly ILogger<Worker> _logger;
    private readonly ITelegramBotClient _client;
    private readonly IServiceProvider _provider;
    private readonly AppDbContext _context;

    public Worker(
        ILogger<Worker> logger,
        IDbContextFactory<AppDbContext> factory,
        ITelegramBotClient client,
        IServiceProvider provider
    )
    {
        _logger = logger;
        _context = factory.CreateDbContext();
        _client = client;
        _provider = provider;
    }

    protected override Task ExecuteAsync(CancellationToken token)
    {
        token.ThrowIfCancellationRequested();

        var channel = _provider.GetRequiredService<IModel>();

        var consumer = new AsyncEventingBasicConsumer(channel);
        consumer.Received += AcceptAndSend;

        channel.BasicConsume("notifications", true, consumer);
        return Task.CompletedTask;
    }

    private async Task AcceptAndSend(object _, BasicDeliverEventArgs args)
    {
        var content = Encoding.UTF8.GetString(args.Body.ToArray());
        var dto = (MessageDTO)JsonSerializer.Deserialize(content, typeof(MessageDTO));

        ArgumentNullException.ThrowIfNull(dto);
        var project = await _context.Projects
            .Where(p => p.Id == dto.Project)
            .Include(p => p.Users)
            .ThenInclude(phu => phu.User)
            .FirstAsync();

        var noty = new Notification
        {
            Project = project,
            Body = dto.Body,
            Severity = dto.Severity,
            Subject = dto.Subject
        };
        await SendNotification(noty);

        await Task.Yield();
    }

    private static string FormatNoty(Notification noty)
    {
        var sb = new StringBuilder();
        sb.Append("<b>[").Append(noty.Project.Title.ToUpper()).Append("]</b>").AppendLine();
        sb.Append("<b>").Append(Emoji(noty.Severity)).Append(noty.Severity).Append(Emoji(noty.Severity))
            .AppendLine("</b>")
            .AppendLine();
        if (!string.IsNullOrEmpty(noty.Subject))
        {
            sb.Append("<b>").Append(noty.Subject).AppendLine("</b>").AppendLine();
        }

        if (!string.IsNullOrEmpty(noty.Body))
        {
            sb.Append("<pre>").Append(noty.Body).Append("</pre>");
        }

        return sb.ToString();
    }

    private static string Emoji(Severity severity)
    {
        return severity switch
        {
            Severity.Trace => "🐾",
            Severity.Debug => "🪲",
            Severity.Information => "ℹ️",
            Severity.Warning => "⚠️",
            Severity.Error => "❗️",
            Severity.Critical => "‼️",
            _ => throw new ArgumentOutOfRangeException(nameof(severity), severity, null)
        };
    }

    private async Task SendNotification(Notification noty, CancellationToken token = default)
    {
        _logger.LogInformation("Start send noty {Subject}", noty.Subject);
        await RefreshUserData(noty, token);
        var text = FormatNoty(noty);
        var disableNotification = noty.Severity <= Severity.Error;

        var usersChunked = FindSuitableUsers(noty);
        var tasks = new List<Task>();
        foreach (var chunk in usersChunked)
        {
            tasks.AddRange(chunk.Select(id => SendMessage(text, id, disableNotification, token)));
            await Task.Delay(1000, token);
        }

        await Task.WhenAll(tasks);

        _logger.LogInformation("Notification sent");
    }

    private async Task RefreshUserData(Notification noty, CancellationToken token = default)
    {
        foreach (var phu in noty.Project.Users)
        {
            await _context.Entry(phu).ReloadAsync(token);
            await _context.Entry(phu.User).ReloadAsync(token);
        }
    }

    private static IEnumerable<long[]> FindSuitableUsers(Notification noty) =>
        noty.Project.Users
            .Where(phu => noty.Severity >= phu.MinimumLevel)
            .Where(phu => phu.User.Authorized)
            .Select(phu => phu.User.TelegramId)
            .Select(long.Parse)
            .Chunk(30)
            .ToList();

    private Task SendMessage(string text, long chatId, bool disableNotification,
        CancellationToken token = default) =>
        _client.SendTextMessageAsync(
            chatId: chatId,
            text: text,
            parseMode: ParseMode.Html,
            disableNotification: disableNotification,
            cancellationToken: token);
}