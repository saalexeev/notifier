using Microsoft.EntityFrameworkCore;
using NotyService.Data;
using Sender;
using Sender.Rabbit;
using Serilog;
using Telegram.Bot;

var host = Host.CreateDefaultBuilder(args)
    .ConfigureLogging((context, builder) =>
    {
        var logger = new LoggerConfiguration()
            .ReadFrom.Configuration(context.Configuration)
            .WriteTo.Console(
                outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u4}][{SourceContext}] {Message:lj}{NewLine}{Exception}"
            ).CreateLogger();

        builder.ClearProviders();
        builder.AddSerilog(logger);
    })
    .ConfigureServices((host, services) =>
    {
        var connectionString = host.Configuration.GetConnectionString("mysql");
        var config = host.Configuration.Get<Configuration>();

        ArgumentException.ThrowIfNullOrEmpty(config.Token);

        services.AddDbContextFactory<AppDbContext>(options =>
            options
                .UseMySql(connectionString, ServerVersion.AutoDetect(config.ConnectionStrings.Mysql),
                    b => b.MigrationsAssembly("NotyService.Data"))
                .UseLazyLoadingProxies()
                .EnableDetailedErrors(host.HostingEnvironment.IsDevelopment())
                .EnableSensitiveDataLogging(host.HostingEnvironment.IsDevelopment())
                .EnableServiceProviderCaching(host.HostingEnvironment.IsProduction())
        );

        services.AddHostedService<Worker>();

        services
            .AddHttpClient("telegram_bot_client")
            .AddTypedClient<ITelegramBotClient>(client =>
                new TelegramBotClient(new TelegramBotClientOptions(config.Token), client)
            );

        services.AddTransient(_ =>
            RabbitChannelFactory.Create(config.Rabbit.Uri, config.Rabbit.Queue).CreateChannel()
        );
    })
    .Build();

await host.RunAsync();