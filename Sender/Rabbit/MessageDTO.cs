using System.Security.Cryptography.X509Certificates;
using NotyService.Data.Entity;

namespace Sender.Rabbit;

public class MessageDTO
{
    public int Project { get; set; }
    public Severity Severity { get; set; }
    public string Subject { get; set; }
    public string Body { get; set; }
}