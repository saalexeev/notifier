using RabbitMQ.Client;
using RabbitMQ.Client.Exceptions;

namespace Sender.Rabbit;

public class RabbitChannelFactory
{
    private readonly string _queueName;
    private IModel _channel;

    private RabbitChannelFactory(Uri uri, string queueName)
    {
        _queueName = queueName;
        var factory = new ConnectionFactory
        {
            Uri = uri,
            DispatchConsumersAsync = true
        };
        var connection = factory.CreateConnection();
        _channel = connection.CreateModel();
    }

    public static RabbitChannelFactory Create(Uri uri, string queueName)
    {
        return new RabbitChannelFactory(uri, queueName)
            .EnsureQueueExists();
    }


    private RabbitChannelFactory EnsureQueueExists()
    {
        _channel!.QueueDeclare(
            queue: _queueName,
            durable: false,
            exclusive: false,
            autoDelete: false,
            arguments: null
        );
        return this;
    }

    internal IModel CreateChannel()
    {
        ArgumentException.ThrowIfNullOrEmpty(nameof(_channel));
        return _channel!;
    }
}