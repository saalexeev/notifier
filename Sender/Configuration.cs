namespace Sender;

public class Configuration
{
    public class AppConfig
    {
        public string Url { get; set; }
    }

    public class RabbitConfig
    {
        public Uri Uri { get; set; }

        public string Queue { get; set; }
    }

    public class DbConfig
    {
        public string Mysql { get; set; }
    }

    public string Token { get; set; }

    public AppConfig App { get; set; }

    public RabbitConfig Rabbit { get; set; }
    public DbConfig ConnectionStrings { get; set; }
}