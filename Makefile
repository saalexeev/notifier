clean:
	@rm -rf /notifier/bin/*

publish-web:
	@dotnet publish WebApi -c Release -o /notifier/bin/web --no-self-contained --nologo --use-current-runtime --disable-build-servers

publish-sender:
	@dotnet publish Sender -c Release -o /notifier/bin/sender --no-self-contained --nologo --use-current-runtime --disable-build-servers
 
run:
	@bin/Sender/Sender &
	@bin/WebApi/WebApi &
	@echo "Started"