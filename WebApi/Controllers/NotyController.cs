using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NotyService.Data;
using NotyService.Data.DTO;
using NotyService.Data.Entity;
using WebApi.Abstract;
using WebApi.Extensions;

namespace WebApi.Controllers;

[Route("api/[controller]")]
[ApiController]
public class NotyController : ControllerBase
{
    private readonly AppDbContext _context;
    private readonly IRabbitMqService _rabbitMqService;

    public NotyController(AppDbContext context, IRabbitMqService rabbitMqService)
    {
        _context = context;
        _rabbitMqService = rabbitMqService;
    }

    [HttpPost]
    public async Task<ActionResult> Send(NotificationDTO dto)
    {
        var project = await _context.Projects.FirstOrDefaultAsync(p => p.Title.ToLower() == dto.Project.ToLower());
        if (project is null)
        {
            return NotFound(new { Error = "Project not found!" });
        }

        if (!Enum.TryParse(dto.Severity, true, out Severity severity))
        {
            return BadRequest(new { Error = $"Severity '{dto.Severity}' not found" });
        }

        await _rabbitMqService.SendMessageAsync(new
        {
            Project = project.Id,
            Severity = severity,
            dto.Subject,
            dto.Body
        });

        return Accepted();
    }
}