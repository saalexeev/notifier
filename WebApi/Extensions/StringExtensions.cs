using NotyService.Data.Entity;

namespace WebApi.Extensions;

public static partial class StringExtensions
{
    public static string EscapeForTelegram(this string subject)
    {
        return subject.Replace("!", @"\!")
            .Replace("_", @"\_");
    }

    public static string Emoji(this Severity severity)
    {
        return severity switch
        {
            Severity.Trace => "🐾",
            Severity.Debug => "🪲",
            Severity.Information => "ℹ️",
            Severity.Warning => "⚠️",
            Severity.Error => "❗️",
            Severity.Critical => "‼️",
            _ => throw new ArgumentOutOfRangeException(nameof(severity), severity, null)
        };
    }
}