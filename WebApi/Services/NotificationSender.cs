using System.Text;
using Microsoft.EntityFrameworkCore;
using NotyService.Data;
using NotyService.Data.Entity;
using TanvirArjel.Extensions.Microsoft.DependencyInjection;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;
using WebApi.Controllers;
using WebApi.Extensions;

namespace WebApi.Services;

public class NotificationSender : BackgroundService
{
    private readonly IServiceProvider _provider;
    private readonly ITelegramBotClient _client;
    private readonly ILogger<NotificationSender> _logger;

    public NotificationSender(IServiceProvider provider, ITelegramBotClient client, ILogger<NotificationSender> logger)
    {
        _provider = provider;
        _client = client;
        _logger = logger;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        using var scope = _provider.CreateScope();
        var ctx = scope.ServiceProvider.GetRequiredService<AppDbContext>();
        while (!stoppingToken.IsCancellationRequested)
        {
            var notifications = await ctx.Notifications
                .Where(n => !n.Sent)
                .ToListAsync(stoppingToken);

            if (!notifications.Any())
            {
                _logger.LogInformation("Noty's not found. Sleeping 5s");
                await Task.Delay(TimeSpan.FromSeconds(5), stoppingToken);
                continue;
            }

            foreach (var notification in notifications)
            {
                var chunks = notification.Project.Users
                    .Where(phu => notification.Severity.CompareTo(phu.MinimumLevel) >= 0)
                    .Chunk(30).ToList();
                var text = FormatNoty(notification);
                var needDelay = chunks.Count > 1;
                foreach (var chunk in chunks)
                {
                    foreach (var projectHasUser in chunk)
                    {
                        await _client.SendTextMessageAsync(
                            chatId: long.Parse(projectHasUser.User.TelegramId),
                            text: text,
                            parseMode: ParseMode.Html,
                            cancellationToken: stoppingToken);
                    }

                    if (needDelay) await Task.Delay(TimeSpan.FromSeconds(1), stoppingToken);
                }

                notification.Sent = true;
                ctx.Update(notification);
            }

            await ctx.SaveChangesAsync(stoppingToken);
        }
    }

    private static string FormatNoty(Notification notification)
    {
        return new StringBuilder()
            .Append("<b>[").Append(notification.Project.Title.ToUpper()).Append("]</b>").AppendLine()
            .Append("<b>").Append(notification.Severity.Emoji()).Append(notification.Severity)
            .Append(notification.Severity.Emoji()).Append("</b>").AppendLine()
            .AppendLine()
            .Append("<b>").Append(notification.Subject).Append("</b>").AppendLine()
            .AppendLine()
            .Append("<pre>").Append(notification.Body).Append("</pre>")
            .ToString();
    }
}