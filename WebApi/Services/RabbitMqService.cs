using System.Text;
using RabbitMQ.Client;
using WebApi.Abstract;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace WebApi.Services;

public class RabbitMqService : IRabbitMqService
{
    private readonly IModel _channel;

    public RabbitMqService(IModel channel)
    {
        _channel = channel;
    }

    public Task SendMessageAsync(object obj)
    {
        var message = JsonSerializer.Serialize(obj);
        var body = Encoding.UTF8.GetBytes(message);

        return SendMessageAsync(body);
    }

    private async Task SendMessageAsync(byte[] message)
    {
        await Task.Run(() => _channel.BasicPublish(
            exchange: "",
            routingKey: "notifications",
            basicProperties: null,
            body: message
        ));
    }
}