using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;
using NotyService.Data;
using RabbitMQ.Client;
using Serilog;
using TanvirArjel.Extensions.Microsoft.DependencyInjection;
using Telegram.Bot;
using WebApi;
using WebApi.Abstract;
using WebApi.AutoMapper;
using WebApi.Services;

var builder = WebApplication.CreateBuilder(args);
var config = builder.Configuration.Get<Configuration>();

builder.Services.AddDbContext<AppDbContext>(options => options
    .UseMySql(config.ConnectionStrings.Mysql, ServerVersion.AutoDetect(config.ConnectionStrings.Mysql))
    .UseLazyLoadingProxies()
    .EnableDetailedErrors(builder.Environment.IsDevelopment())
    .EnableSensitiveDataLogging(builder.Environment.IsDevelopment())
    .EnableServiceProviderCaching(builder.Environment.IsProduction())
);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var logger = new LoggerConfiguration()
    .ReadFrom.Configuration(builder.Configuration)
    .Enrich.FromLogContext()
    .WriteTo.Console(
        outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u4}][{SourceContext}] {Message:lj}{NewLine}{Exception}"
    )
    .CreateLogger();

builder.Logging.ClearProviders();
builder.Logging.AddSerilog(logger);

var token = builder.Configuration.GetRequiredSection("Token").Get<string>();
ArgumentException.ThrowIfNullOrEmpty(token, nameof(token));

builder.Services.AddHttpClient("telegram_bot_client")
    .AddTypedClient<ITelegramBotClient>(client =>
    {
        var options = new TelegramBotClientOptions(config.Token);
        return new TelegramBotClient(options, client);
    });

builder.Services.AddScoped(_ =>
{
    var factory = new ConnectionFactory
    {
        Uri = config.Rabbit.Uri
    };

    return factory.CreateConnection().CreateModel();
});

builder.Services.AddScoped<IRabbitMqService, RabbitMqService>();
builder.Services.AddSingleton(_ => config);

builder.Services.AddServicesOfAllTypes();
builder.Services.AddAutoMapper(typeof(AppMappingProfile));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

using (var scope = app.Services.CreateScope())
{
    await scope.ServiceProvider.GetRequiredService<AppDbContext>()
        .Database
        .MigrateAsync();
}

if (app.Environment.IsProduction())
{
    app.UseForwardedHeaders(new ForwardedHeadersOptions
    {
        ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
    });
}

app.Run(config.App.Url);