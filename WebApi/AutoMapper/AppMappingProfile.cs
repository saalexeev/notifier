using AutoMapper;
using NotyService.Data.DTO;
using NotyService.Data.Entity;

namespace WebApi.AutoMapper;

public class AppMappingProfile : Profile
{
    public AppMappingProfile()
    {
        CreateMap<ProjectDto, Project>().ReverseMap();
    }
}