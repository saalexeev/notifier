namespace WebApi.Abstract;

public interface IRabbitMqService
{
    Task SendMessageAsync(object message);
}