namespace WebApi;

public class Configuration
{
    public class AppConfig
    {
        public string Url { get; init; }
    }

    public class RabbitConfig
    {
        public Uri Uri { get; init; }

        public string Queue { get; init; }
    }

    public class DbConfig
    {
        public string Mysql { get; init; }
    }

    public string Token { get; init; }

    public AppConfig App { get; init; }

    public RabbitConfig Rabbit { get; init; }
    public DbConfig ConnectionStrings { get; init; }

    public string AuthToken { get; init; }
}