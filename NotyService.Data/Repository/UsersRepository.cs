using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using NotyService.Data.Abstract;
using NotyService.Data.Entity;
using TanvirArjel.Extensions.Microsoft.DependencyInjection;

namespace NotyService.Data.Repository;

[ScopedService]
public class UsersRepository : RepositoryBase<User>
{
    public UsersRepository(AppDbContext context, ILogger<UsersRepository> logger) : base(context, logger)
    {
    }

    public async Task<Result<User>> FindByTelegramIdAsync(string id)
    {
        var user = await Context.Users.Where(u => u.TelegramId == id).FirstOrDefaultAsync();
        await Context.Entry(user).ReloadAsync();
        return Result.Ok(user);
    }
    
}