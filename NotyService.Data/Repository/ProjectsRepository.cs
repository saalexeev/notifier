using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using NotyService.Data.Abstract;
using NotyService.Data.Entity;
using TanvirArjel.Extensions.Microsoft.DependencyInjection;

namespace NotyService.Data.Repository;

[ScopedService]
public class ProjectsRepository : RepositoryBase<Project>
{
    public ProjectsRepository(AppDbContext context, ILogger<ProjectsRepository> logger) : base(context, logger)
    {
    }

    public async Task<Result<Project>> FindByTitleAsync(string title)
    {
        var project = await Context.Projects.Where(p => p.Title == title).FirstOrDefaultAsync();
        return Result.Ok(project);
    }
}