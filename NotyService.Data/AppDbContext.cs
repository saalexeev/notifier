using Microsoft.EntityFrameworkCore;
using NotyService.Data.Entity;
using Project = NotyService.Data.Entity.Project;

namespace NotyService.Data;

public class AppDbContext : DbContext
{
    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
    {
    }

    public DbSet<Project> Projects { get; set; }

    public DbSet<User> Users { get; set; }

    public DbSet<Notification> Notifications { get; set; }

    public DbSet<ProjectHasUser> ProjectHasUsers { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Project>(entity =>
        {
            entity.HasIndex(p => p.Title)
                .IsUnique();
        });

        modelBuilder.Entity<ProjectHasUser>(entity =>
        {
            entity.HasKey(phu => new { phu.ProjectId, phu.UserId });
            entity.HasOne(phu => phu.Project)
                .WithMany(p => p.Users)
                .HasForeignKey(phu => phu.ProjectId);
            entity.HasOne(phu => phu.User)
                .WithMany(u => u.Projects)
                .HasForeignKey(phu => phu.UserId);
        });


        modelBuilder.Entity<Notification>(entity =>
        {
            entity.HasOne(n => n.Project)
                .WithMany(p => p.Notifications)
                .HasForeignKey(n => n.ProjectId)
                .IsRequired();
        });

        modelBuilder.Entity<User>(entity =>
        {
            entity.HasIndex(u => u.TelegramId)
                .IsUnique();
        });
    }
}