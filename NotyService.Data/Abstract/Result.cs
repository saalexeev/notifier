using System.ComponentModel.Design;

namespace NotyService.Data.Abstract;

public class Result
{
    public bool Success { get; }
    public string Error { get; }

    public Exception Exception { get; }

    public bool IsFailure => !Success;

    protected Result(bool success, string error)
    {
        if (success && error != string.Empty)
            throw new InvalidOperationException();
        if (!success && error == string.Empty)
            throw new InvalidOperationException();

        Success = success;
        Error = error;
    }

    protected Result(Exception ex)
    {
        Success = false;
        Exception = ex;
        Error = ex.Message;
    }

    public static Result Fail(string error) => new(false, error);

    public static Result Fail(Exception ex) => new(ex);

    public static Result<T> Fail<T>(string error) => new(default(T), false, error);

    public static Result<T> Fail<T>(Exception ex) => new(default(T), ex);

    public static Result Ok() => new(true, string.Empty);

    public static Result<T> Ok<T>(T value) => new(value, true, string.Empty);
}

public class Result<T> : Result
{
    public T Value { get; set; }

    protected internal Result(T value, bool success, string error) : base(success, error)
    {
        Value = value;
    }

    protected internal Result(T value, Exception ex) : base(ex)
    {
        Value = value;
    }
}