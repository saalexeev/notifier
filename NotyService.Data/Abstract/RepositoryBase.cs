using System.Data.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace NotyService.Data.Abstract;

public abstract class RepositoryBase<TEntity, TEntityKey> where TEntity : class
{
    protected readonly AppDbContext Context;
    protected readonly ILogger<RepositoryBase<TEntity, TEntityKey>> Logger;

    protected RepositoryBase(AppDbContext context, ILogger<RepositoryBase<TEntity, TEntityKey>> logger)
    {
        Context = context;
        Logger = logger;
    }

    public async Task<Result<bool>> ExistsAsync(TEntityKey key)
    {
        var entity = await Context.Set<TEntity>().FindAsync(key);
        if (entity is null)
        {
            return Result.Ok(false);
        }

        Context.Entry(entity).State = EntityState.Detached;
        return Result.Ok(true);
    }

    public async Task<Result<bool>> ExistsAsync(TEntityKey key, CancellationToken token) =>
        Result.Ok(await Context.Set<TEntity>().FindAsync(new object[] { key }, cancellationToken: token) is not null);

    public async Task<Result<IEnumerable<TEntity>>> GetListAsync() =>
        Result.Ok<IEnumerable<TEntity>>(await Context.Set<TEntity>().ToListAsync());

    public async Task<Result<IEnumerable<TEntity>>> GetListAsync(CancellationToken token) =>
        Result.Ok<IEnumerable<TEntity>>(await Context.Set<TEntity>().ToListAsync(token));

    public async Task<Result<TEntity>> FindAsync(TEntityKey key) =>
        Result.Ok(await Context.FindAsync<TEntity>(key));

    public async Task<Result<TEntity>> FindAsync(TEntityKey key, CancellationToken token) =>
        Result.Ok(await Context.FindAsync<TEntity>(key, token));

    public async Task<Result> CreateAsync(TEntity entity)
    {
        Context.Set<TEntity>().Add(entity);

        try
        {
            await Context.SaveChangesAsync();
        }
        catch (DbUpdateException ex)
        {
            Logger.LogError("{Exception}", ex);
            return Result.Fail(ex);
        }

        return Result.Ok();
    }

    public async Task<Result> CreateAsync(TEntity entity, CancellationToken token)
    {
        Context.Set<TEntity>().Add(entity);

        try
        {
            await Context.SaveChangesAsync(cancellationToken: token);
        }
        catch (DbUpdateException ex)
        {
            Logger.LogError("{Exception}", ex);
            return Result.Fail(ex);
        }

        return Result.Ok();
    }

    public async Task<Result> UpdateAsync(TEntity entity)
    {
        Context.Update(entity);

        try
        {
            await Context.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException ex)
        {
            Logger.LogError("{Exception}", ex);
            return Result.Fail(ex);
        }

        return Result.Ok();
    }

    public async Task<Result> UpdateAsync(TEntity entity, CancellationToken token)
    {
        Context.Update(entity);

        try
        {
            await Context.SaveChangesAsync(token);
        }
        catch (DbUpdateConcurrencyException ex)
        {
            Logger.LogError("{Exception}", ex);
            return Result.Fail(ex);
        }

        return Result.Ok();
    }

    public async Task<Result> DeleteAsync(TEntity entity)
    {
        Context.Remove(entity);
        try
        {
            await Context.SaveChangesAsync();
        }
        catch (DbException ex)
        {
            Logger.LogError("{Exception}", ex);
            return Result.Fail(ex);
        }

        return Result.Ok();
    }

    public async Task<Result> DeleteAsync(TEntity entity, CancellationToken token)
    {
        Context.Remove(entity);
        try
        {
            await Context.SaveChangesAsync(cancellationToken: token);
        }
        catch (DbException ex)
        {
            Logger.LogError("{Exception}", ex);
            return Result.Fail(ex);
        }

        return Result.Ok();
    }
}

public abstract class RepositoryBase<TEntity> : RepositoryBase<TEntity, int> where TEntity : class
{
    protected RepositoryBase(AppDbContext context, ILogger<RepositoryBase<TEntity, int>> logger)
        : base(context, logger)
    {
    }
}