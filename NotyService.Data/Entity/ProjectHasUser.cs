using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;

namespace NotyService.Data.Entity;

[Table("projects_has_users")]
public class ProjectHasUser
{
    [Column("project_id")]
    public int ProjectId { get; set; }
    public virtual Project Project { get; set; }

    [Column("user_id")]
    public int UserId { get; set; }
    public virtual User User { get; set; }

    [Column("minimum_level")]
    public Severity MinimumLevel { get; set; } = Severity.Information;
}