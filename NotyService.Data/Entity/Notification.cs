using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace NotyService.Data.Entity;

[Table("notifications")]
[PrimaryKey(nameof(Id))]
public class Notification
{
    [Column("id")]
    public int Id { get; set; }

    [Column("project_id")]
    public int ProjectId { get; set; }
    
    public virtual Project Project { get; set; }

    [Required]
    [Column("severity")]
    public Severity Severity { get; set; }

    [Column("subject", TypeName = "varchar(200)")]
    public string Subject { get; set; }

    [Required]
    [Column("body")]
    public string Body { get; set; }

    [Column("sent")]
    public bool Sent { get; set; } = false;
}

public enum Severity
{
    Trace,
    Debug,
    Information,
    Warning,
    Error,
    Critical
}