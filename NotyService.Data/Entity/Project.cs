using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace NotyService.Data.Entity;

[Table("projects")]
[PrimaryKey(nameof(Id))]
public class Project
{
    [Column("id")]
    public int Id { get; set; }

    [Column("title", TypeName = "varchar(200)")]
    public string Title { get; set; }

    public virtual List<ProjectHasUser> Users { get; set; } = new();

    public virtual List<Notification> Notifications { get; set; } = new();
}