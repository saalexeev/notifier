using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace NotyService.Data.Entity;

[Table("users")]
[PrimaryKey(nameof(Id))]
public class User
{
    [Column("id")]
    public int Id { get; set; }
    
    [Column("telegram_id", TypeName = "varchar(50)")]
    public string TelegramId { get; set; }
    
    [Column("first_name", TypeName = "varchar(250)")]
    public string FirstName { get; set; }

    [Column("username", TypeName = "varchar(250)")]
    public string Username { get; set; }
    
    public virtual List<ProjectHasUser> Projects { get; set; } = new();

    public bool Authorized { get; set; } = false;
}