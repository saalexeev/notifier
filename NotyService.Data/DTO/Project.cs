namespace NotyService.Data.DTO;

public class ProjectDto
{
    public int Id { get; set; }
    public string Title { get; set; }
}