using System.Runtime;

namespace NotyService.Data.DTO;

public class NotificationDTO
{
    public string Project { get; set; }
    public string Severity { get; set; }
    public string Subject { get; set; }
    public string Body { get; set; }
}