﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace NotyService.Data.Migrations
{
    /// <inheritdoc />
    public partial class IsAuthorizedFlag : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Authorized",
                table: "users",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Authorized",
                table: "users");
        }
    }
}
