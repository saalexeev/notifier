using Bot.Abstract;
using Bot.Attributes;
using Bot.Services;
using NotyService.Data.Entity;
using NotyService.Data.Repository;
using TanvirArjel.Extensions.Microsoft.DependencyInjection;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Bot.CommandHandlers;

[ScopedService]
[CommandHandler(
    Command = "unsub", 
    Description = "Отписаться от уведомлений проекта",
    RequireAuth = true,
    Usage = @"<Название проекта> - проект, от которого ты хочешь отписаться
Например: /unsub ProjectName"
)]
public class UnsubscribeCommand : CommandHandlerBase
{
    private readonly ProjectsRepository _projectsRepository;
    private readonly UserService _userService;

    public UnsubscribeCommand(
        ITelegramBotClient client,
        ProjectsRepository projectsRepository,
        UserService userService
    ) : base(client, userService)
    {
        _projectsRepository = projectsRepository;
        _userService = userService;
    }

    protected override async Task Handle(Message message, CancellationToken token)
    {
        var command = GetCommandFrom(message.Entities);
        var arguments = message.Text![(command.Offset + command.Length)..].Trim();

        Project project;
        try
        {
            project = await ValidateProject(arguments);
        }
        catch (ArgumentException ex)
        {
            await Client.SendTextMessageAsync(
                chatId: message.Chat.Id,
                text: ex.Message + ": не подходит",
                cancellationToken: token);
            return;
        }

        var user = await _userService.FetchUser(message.Chat);

        var phu = project.Users.First(phu => phu.User.Id == user.Id);
        project.Users.Remove(phu);
        
        await _projectsRepository.UpdateAsync(project, token);

        await Client.SendTextMessageAsync(
            chatId: message.Chat.Id,
            text: "Отписал тебя от проекта *" + project.Title + '*',
            parseMode: ParseMode.MarkdownV2,
            cancellationToken: token
        );
    }

    private async Task<Project> ValidateProject(string projectName)
    {
        var project = await _projectsRepository.FindByTitleAsync(projectName);
        if (project.Value is null)
        {
            throw new ArgumentException("Проект не найден", nameof(project));
        }

        return project.Value;
    }
}