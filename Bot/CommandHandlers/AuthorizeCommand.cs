using Bot.Abstract;
using Bot.Attributes;
using Bot.Services;
using Microsoft.Extensions.Configuration;
using TanvirArjel.Extensions.Microsoft.DependencyInjection;
using Telegram.Bot;
using Telegram.Bot.Types;
using User = NotyService.Data.Entity.User;

namespace Bot.CommandHandlers;

[ScopedService]
[CommandHandler(
    Command = "auth",
    RequireAuth = false,
    Description = "Авторизация в боте, за токеном обращаться к тому, кто дал ссылку на бота",
    Usage = "/auth <token>"
)]
public class AuthorizeCommand : CommandHandlerBase
{
    private readonly UserService _userService;
    private readonly string _authToken;

    public AuthorizeCommand(
        ITelegramBotClient client, 
        IConfiguration config, 
        UserService userService) : base(client, userService)
    {
        _userService = userService;
        _authToken = config["AuthToken"];
    }

    protected override async Task Handle(Message message, CancellationToken token)
    {
        var command = GetCommandFrom(message.Entities);
        var givenToken = message.Text![(command.Offset + command.Length)..].Trim();

        var user = await _userService.FetchUser(message.Chat);
        if (user.Authorized)
        {
            await AlreadyAuthorized(user, token);
            return;
        }

        if (!givenToken.Equals(_authToken))
        {
            await InvalidToken(user, token);
            return;
        }

        await _userService.MarkAuthorized(user);
        await Success(user, token);
    }

    private Task AlreadyAuthorized(User user, CancellationToken token = default)
    {
        return Client.SendTextMessageAsync(
            chatId: long.Parse(user.TelegramId),
            text: "Ты уже авторизован",
            cancellationToken: token
        );
    }

    private Task InvalidToken(User user, CancellationToken token = default)
    {
        return Client.SendTextMessageAsync(
            chatId: long.Parse(user.TelegramId),
            text: "Введен неправильный токен",
            cancellationToken: token);
    }

    private Task Success(User user, CancellationToken token = default)
    {
        return Client.SendTextMessageAsync(
            chatId: long.Parse(user.TelegramId),
            text: "Успешная авторизация",
            cancellationToken: token);
    }
}