using System.Text;
using Bot.Abstract;
using Bot.Attributes;
using Bot.Services;
using NotyService.Data.Repository;
using TanvirArjel.Extensions.Microsoft.DependencyInjection;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Bot.CommandHandlers;

[ScopedService]
[CommandHandler(
    Command = "myprojects",
    Description = "Покажу все проекты, на которые ты подписан", 
    RequireAuth = true
)]
public class MyProjectsCommand : CommandHandlerBase
{
    private readonly UserService _userService;

    public MyProjectsCommand(
        ITelegramBotClient client,
        UserService userService
    ) : base(client, userService)
    {
        _userService = userService;
    }

    protected override async Task Handle(Message message, CancellationToken token)
    {
        var user = await _userService.FetchUser(message.Chat);
        if (!user.Authorized)
        {
            await Unauthorized(user, token);
            return;
        }

        var reply = new StringBuilder();

        if (user.Projects.Count > 0)
        {
            reply.AppendLine("Проекты на которые ты подписан:").AppendLine();
            foreach (var phu in user.Projects)
            {
                reply.Append("⬛️ *").Append(phu.Project.Title).Append('*')
                    .Append(" с уровнем: *").Append(phu.MinimumLevel).Append('*')
                    .AppendLine();
            }    
        }
        else
        {
            reply.AppendLine("Ты не подписан ни на один проект");
        }
        

        await Client.SendTextMessageAsync(
            chatId: message.Chat.Id,
            text: reply.ToString(),
            parseMode: ParseMode.MarkdownV2,
            cancellationToken: token);
    }
}