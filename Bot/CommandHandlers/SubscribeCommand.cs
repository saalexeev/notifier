using System.Text;
using System.Text.RegularExpressions;
using Bot.Abstract;
using Bot.Attributes;
using Bot.Services;
using NotyService.Data.Entity;
using NotyService.Data.Repository;
using TanvirArjel.Extensions.Microsoft.DependencyInjection;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Bot.CommandHandlers;

[ScopedService]
[CommandHandler(
    Command = "sub",
    Description = "Подписаться на уведомления от проекта",
    RequireAuth = true,
    Usage = @"Аргументы:
<Название проекта> - название проекта, полученное от команды /projects
<Важность уведомлений> - можно получить список доступных значений из команды /severities
Пример: /sub ProjectName Information"
)]
public class SubscribeCommand : CommandHandlerBase
{
    private readonly ProjectsRepository _projectsRepository;
    private readonly UserService _usersService;

    public SubscribeCommand(
        ITelegramBotClient client,
        ProjectsRepository projectsRepository,
        UserService usersService
    ) : base(client, usersService)
    {
        _projectsRepository = projectsRepository;
        _usersService = usersService;
    }

    protected override async Task Handle(Message message, CancellationToken token)
    {
        var command = GetCommandFrom(message.Entities);
        var arguments = message.Text![(command.Offset + command.Length)..].Trim();

        var matches = Regex.Match(arguments, "(?<projectName>.*)\\s(?<severity>[\\w]+)").Groups;
        if (matches.Count < 2)
        {
            await Client.SendTextMessageAsync(
                chatId: message.Chat.Id,
                text: "Неверное количество аргументов. Ожидалось 2, получено: " + matches.Count,
                cancellationToken: token);
            return;
        }

        Project project;
        Severity severity;
        try
        {
            project = await ValidateProject(matches["projectName"].Value);
            severity = ValidateSeverity(matches["severity"].Value);
        }
        catch (ArgumentException ex)
        {
            await Client.SendTextMessageAsync(
                chatId: message.Chat.Id,
                text: ex.Message + ": не подходит",
                cancellationToken: token);
            return;
        }

        var user = await _usersService.FetchUser(message.Chat);

        if (project.Users.Any(phu => phu.User.Id == user.Id))
        {
            project.Users.Find(phu => phu.User.Id == user.Id).MinimumLevel = severity;
        }
        else
        {
            project.Users.Add(new ProjectHasUser { Project = project, User = user, MinimumLevel = severity });
        }

        await _projectsRepository.UpdateAsync(project, token);

        var reply = new StringBuilder();
        reply.Append("Подписал тебя на проект ").Append('*').Append(project.Title).Append('*').AppendLine()
            .AppendLine()
            .Append("Ты будешь получать уведомления только с важностью ").Append('*').Append(severity).Append('*')
            .Append(" и выше");

        await Client.SendTextMessageAsync(
            chatId: message.Chat.Id,
            text: reply.ToString(),
            parseMode: ParseMode.MarkdownV2,
            cancellationToken: token);
    }

    private static Severity ValidateSeverity(string severity)
    {
        if (!Enum.TryParse(severity, true, out Severity enumValue))
        {
            throw new ArgumentException("Уровень важности не найден", nameof(severity));
        }

        return enumValue;
    }

    private async Task<Project> ValidateProject(string projectName)
    {
        var project = await _projectsRepository.FindByTitleAsync(projectName);
        if (project.Value is null)
        {
            throw new ArgumentException("Проект не найден", nameof(project));
        }

        return project.Value;
    }
}