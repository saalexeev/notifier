using Bot.Abstract;
using Bot.Attributes;
using Bot.Services;
using Microsoft.Extensions.Logging;
using TanvirArjel.Extensions.Microsoft.DependencyInjection;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace Bot.CommandHandlers;

[ScopedService]
[CommandHandler(
    Command = "about",
    Description = "Информация о боте",
    RequireAuth = false
)]
public class AboutCommand : CommandHandlerBase
{
    private readonly ILogger<AboutCommand> _logger;

    public AboutCommand(
        ITelegramBotClient client,
        UserService userService,
        ILogger<AboutCommand> logger) : base(client, userService)
    {
        _logger = logger;
    }

    protected override Task Handle(Message message, CancellationToken token)
    {
        _logger.LogInformation("About command");
        const string text = @"
МАКСИМАЛьные уведомления
Получай уведомления, отфильтрованные по важности, от проектов, на которые ты подписан!
‼️Будь информирован о жизни своего проекта!
";
        var task = Client.SendTextMessageAsync(
            chatId: message.Chat.Id,
            text: text,
            cancellationToken: token);

        return task;
    }
}