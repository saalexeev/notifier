using System.Net;
using System.Net.Sockets;
using Bot.Abstract;
using Bot.Attributes;
using Bot.Services;
using TanvirArjel.Extensions.Microsoft.DependencyInjection;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Bot.CommandHandlers;

[ScopedService]
[CommandHandler(
    Command = "api",
    Description = "Описание API",
    RequireAuth = true
)]
public class ApiCommand : CommandHandlerBase
{
    public ApiCommand(
        ITelegramBotClient client,
        UserService userService
    ) : base(client, userService)
    {
    }

    protected override Task Handle(Message message, CancellationToken token) =>
        Client.SendTextMessageAsync(
            chatId: message.Chat.Id,
            text: GetApiText(),
            parseMode: ParseMode.Html,
            cancellationToken: token
        );

    private static string GetLocalIpAddress()
    {
        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (var ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                return ip.ToString();
            }
        }

        throw new Exception("No network adapters with an IPv4 address in the system!");
    }

    private static string GetApiText() => @$"
Host: {GetLocalIpAddress()}
Port: 5047

Send notification:

POST http://{GetLocalIpAddress()}:5047/api/noty
Content-Type: application/json
<code>{{
    ""project"": ""name-of-existing-project(required)"",
    ""severity"": ""level-of-severity(required)"",
    ""subject"": ""subject-of-message(may be empty or skipped)"",
    ""body"": ""body-of-message(may be empty or skipped)""
}}</code>

Successful response:
HTTP 202 Accepted
(empty response)

Error responses:
HTTP 404 Not Found - <i>when given project not found</i>
<code>{{
    ""error"": ""Project not found!""
}}</code>

HTTP 400 Bad Request - <i>when given severity not in severities list</i> 
<code>{{
    ""error"": ""Severity 'sent-severity' not found!""
}}</code>
";
}