using Bot.Abstract;
using Bot.Attributes;
using Bot.Services;
using Microsoft.Extensions.Logging;
using TanvirArjel.Extensions.Microsoft.DependencyInjection;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace Bot.CommandHandlers;

[ScopedService]
[CommandHandler(
    Command = "start",
    Description = "Start bot",
    RequireAuth = false
)]
public class StartCommand : CommandHandlerBase
{
    private readonly ILogger<StartCommand> _logger;
    private readonly UserService _userService;

    public StartCommand(
        ITelegramBotClient client,
        UserService userService,
        ILogger<StartCommand> logger) : base(client, userService)
    {
        _logger = logger;
        _userService = userService;
    }

    protected override async Task Handle(Message message, CancellationToken token)
    {
        var user = await _userService.FetchUser(message.Chat);
        _logger.LogInformation("Start command");
        await Client.SendTextMessageAsync(
            chatId: message.Chat.Id,
            text: "Ну привет, " + user.FirstName + "!",
            cancellationToken: token
        );
    }
}