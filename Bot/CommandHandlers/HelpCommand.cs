using System.Reflection;
using System.Text;
using Bot.Abstract;
using Bot.Attributes;
using Bot.Services;
using Microsoft.Extensions.Logging;
using TanvirArjel.Extensions.Microsoft.DependencyInjection;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Bot.CommandHandlers;

[ScopedService]
[CommandHandler(
    Command = "help",
    RequireAuth = false,
    Description = "Показать доступные команды. Для информации о конкретной команде используй /help <command>",
    Usage = "/help <command> Показать использование данной команды, например /help start"
)]
public class HelpCommand : CommandHandlerBase
{
    private readonly ILogger<HelpCommand> _logger;

    public HelpCommand(
        ITelegramBotClient client, 
        UserService userService,
        ILogger<HelpCommand> logger) : base(client, userService)
    {
        _logger = logger;
    }

    protected override Task Handle(Message message, CancellationToken token)
    {
        if (message.Text is not { } messageText)
        {
            return Task.CompletedTask;
        }

        return messageText == "/help" ? ShowFullHelp(message, token) : ShowPartialHelp(message, token);
    }

    private async Task ShowPartialHelp(Message message, CancellationToken token)
    {
        if (message.Text is not { } messageText) return;
        _logger.LogInformation("Partial help command");

        var command = GetCommandFromMessage(message);
        var argument = messageText[(command.Offset + command.Length)..].Trim().Split(' ')[0].Trim();
        _logger.LogInformation("{arg}", argument);
        var metadata = await BuildMetadata(message);

        if (metadata.TryGetValue(argument, out var value))
        {
            await Client.SendTextMessageAsync(
                chatId: message.Chat.Id,
                text: value.Usage,
                cancellationToken: token);
            return;
        }

        await Client.SendTextMessageAsync(
            chatId: message.Chat.Id,
            text: $"Не знаю команду {argument}",
            cancellationToken: token);

        await ShowFullHelp(message, token);
    }

    private async Task ShowFullHelp(Message message, CancellationToken token)
    {
        _logger.LogInformation("Full help command");
        var metadata = await BuildMetadata(message);

        var reply = new StringBuilder();
        reply.AppendLine("Доступные команды бота:");
        foreach (var item in metadata)
        {
            reply.AppendLine($"🤖 /{item.Value.Command} {item.Value.Description}");
        }

        await Client.SendTextMessageAsync(
            chatId: message.Chat.Id,
            text: reply.ToString(),
            cancellationToken: token);
    }

    private async Task<Dictionary<string, HandlerMetadata>> BuildMetadata(Message message)
    {
        var metadata = new Dictionary<string, HandlerMetadata>();
        var handlers = GetType().Assembly.DefinedTypes
            .Where(t => t is { IsClass: true, IsPublic: true })
            .Where(t => t.ImplementedInterfaces.Contains(typeof(ICommandHandler)))
            .Where(t => t.GetCustomAttribute<CommandHandlerAttribute>() is not null);
        foreach (var handler in handlers)
        {
            var attr = handler.AsType().GetCustomAttribute<CommandHandlerAttribute>()!;
            var isAuthorized = await IsAuthorized(message);
            if (isAuthorized || !attr.RequireAuth)
            {
                metadata.Add(attr.Command, new HandlerMetadata(attr));   
            }
        }

        return metadata;
    }

    private MessageEntity GetCommandFromMessage(Message message)
    {
        ArgumentNullException.ThrowIfNull(message.Entities);
        return message.Entities.First(e => e.Type is MessageEntityType.BotCommand);
    }

    private struct HandlerMetadata
    {
        public string Command { get; set; }
        public string Description { get; set; }
        public string Usage { get; set; }

        public HandlerMetadata(CommandHandlerAttribute attr)
        {
            Command = attr.Command;
            Description = attr.Description;
            Usage = attr.Usage ?? $"У команды нет аргументов";
        }
    }
}