using Bot.Abstract;
using Bot.Attributes;
using Bot.Services;
using NotyService.Data.Repository;
using TanvirArjel.Extensions.Microsoft.DependencyInjection;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace Bot.CommandHandlers;

[ScopedService]
[CommandHandler(
    Command = "del",
    Description = "Удалить проект",
    RequireAuth = true,
    Usage = "/del <title>"
)]
public class DelProjectCommand : CommandHandlerBase
{
    private readonly ProjectsRepository _projectsRepository;

    public DelProjectCommand(
        ITelegramBotClient client,
        ProjectsRepository projectsRepository,
        UserService userService
    ) : base(client, userService)
    {
        _projectsRepository = projectsRepository;
    }

    protected override async Task Handle(Message message, CancellationToken token)
    {
        var command = GetCommandFrom(message.Entities);
        var projectTitle = message.Text![(command.Offset + command.Length)..].Trim();

        var projectResult = await _projectsRepository.FindByTitleAsync(projectTitle);

        if (projectResult.Value is null)
        {
            await Client.SendTextMessageAsync(
                chatId: message.Chat.Id,
                text: "Проект с таким названием не найден",
                cancellationToken: token
            );
            return;
        }

        await _projectsRepository.DeleteAsync(projectResult.Value, token);
        await Client.SendTextMessageAsync(
            chatId: message.Chat.Id,
            text: "Проект удален",
            cancellationToken: token);
    }
}