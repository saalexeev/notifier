using System.Net.Mime;
using System.Text;
using Bot.Abstract;
using Bot.Attributes;
using Bot.Services;
using NotyService.Data.Entity;
using TanvirArjel.Extensions.Microsoft.DependencyInjection;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Bot.CommandHandlers;

[ScopedService]
[CommandHandler(
    Command = "severities",
    Description = "Список значений важности уведомлений",
    RequireAuth = true
)]
public class SeveritiesCommand : CommandHandlerBase
{
    public SeveritiesCommand(ITelegramBotClient client, UserService userService) : base(client, userService)
    {
    }

    protected override async Task Handle(Message message, CancellationToken token)
    {
        var map = new Dictionary<string, Severity>
        {
            { "Trace", Severity.Trace },
            { "Debug", Severity.Debug },
            { "Information", Severity.Information },
            { "Warning", Severity.Warning },
            { "Error", Severity.Error },
            { "Critical", Severity.Critical },
        };
        var text = new StringBuilder();
        text.AppendLine("Список доступных значений важности уведомлений:").AppendLine();
        foreach (var item in map)
        {
            text.Append("⬛️ *`").Append(item.Key).Append("`*").AppendLine();
        }
        
        await Client.SendTextMessageAsync(
            chatId: message.Chat.Id,
            text: text.ToString(),
            parseMode: ParseMode.MarkdownV2,
            cancellationToken: token);
    }
}