using System.Text;
using Bot.Abstract;
using Bot.Attributes;
using Bot.Services;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using NotyService.Data.Repository;
using TanvirArjel.Extensions.Microsoft.DependencyInjection;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Bot.CommandHandlers;

[ScopedService]
[CommandHandler(Command = "projects", Description = "Список доступных проектов", RequireAuth = true)]
public class ProjectsCommand : CommandHandlerBase
{
    private readonly ILogger<ProjectsCommand> _logger;
    private readonly ProjectsRepository _repository;

    public ProjectsCommand(
        ITelegramBotClient client, 
        ProjectsRepository repository,
        UserService userService,
        ILogger<ProjectsCommand> logger) : base(client, userService) 
    {
        _logger = logger;
        _repository = repository;
    }

    protected override async Task Handle(Message message, CancellationToken token)
    {
        var listResult = await _repository.GetListAsync(token);
        var projects = new StringBuilder();
        projects.AppendLine("Ты можешь подписаться на получение уведомлений от следующих проектов:")
            .AppendLine();
        foreach (var item in listResult.Value)
        {
            projects.Append("⬛️ ").Append('*').Append(item.Title).Append('*').AppendLine();
        }

        projects.AppendLine().AppendLine("Для того, чтобы подписаться, используй команду /sub \\<Название проекта\\>");
        
        await Client.SendTextMessageAsync(
            parseMode: ParseMode.MarkdownV2,
            chatId: message.Chat.Id,
            text: projects.ToString(),
            cancellationToken: token);
    }
}