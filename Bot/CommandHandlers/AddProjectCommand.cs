using Bot.Abstract;
using Bot.Attributes;
using Bot.Services;
using NotyService.Data.Entity;
using NotyService.Data.Repository;
using TanvirArjel.Extensions.Microsoft.DependencyInjection;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace Bot.CommandHandlers;

[ScopedService]
[CommandHandler(
    Command = "add",
    Description = "Добавить проект",
    RequireAuth = true,
    Usage = "/add <title>"
)]
public class AddProjectCommand : CommandHandlerBase
{
    private readonly ProjectsRepository _projectsRepository;

    public AddProjectCommand(ITelegramBotClient client, UserService userService,
        ProjectsRepository projectsRepository) : base(client, userService)
    {
        _projectsRepository = projectsRepository;
    }

    protected override async Task Handle(Message message, CancellationToken token)
    {
        var command = GetCommandFrom(message.Entities);
        var projectTitle = message.Text![(command.Offset + command.Length)..].Trim();

        var projectResult = await _projectsRepository.FindByTitleAsync(projectTitle);

        if (projectResult.Value is not null)
        {
            await Client.SendTextMessageAsync(
                chatId: message.Chat.Id,
                text: "Проект с таким названием уже существует",
                cancellationToken: token
            );
            return;
        }

        await _projectsRepository.CreateAsync(new Project { Title = projectTitle }, token);

        await Client.SendTextMessageAsync(
            chatId: message.Chat.Id,
            text: "Проект создан",
            cancellationToken: token);
    }
}