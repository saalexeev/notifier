using Bot.Abstract;
using Microsoft.Extensions.Logging;
using TanvirArjel.Extensions.Microsoft.DependencyInjection;

namespace Bot.Services;

[HostedService]
public class PollingService : PollingServiceBase<ReceiverService>
{
    public PollingService(
        IServiceProvider serviceProvider,
        ILogger<PollingService> logger) : base(serviceProvider, logger)
    {
    }
}