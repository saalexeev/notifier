using System.Reflection;
using Bot.Abstract;
using Bot.Attributes;
using Microsoft.Extensions.DependencyInjection;
using TanvirArjel.Extensions.Microsoft.DependencyInjection;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace Bot.Services;

[ScopedService]
public class CommandHandler
{
    private readonly IServiceProvider _provider;

    private Dictionary<string, ICommandHandler> Handlers { get; } = new();

    public CommandHandler(IServiceProvider provider)
    {
        _provider = provider;
        RegisterHandlers();
    }

    private void RegisterHandlers()
    {
        var handlers = GetType().Assembly.DefinedTypes
            .Where(t => t is { IsClass: true, IsPublic: true })
            .Where(t => t.ImplementedInterfaces.Contains(typeof(ICommandHandler)))
            .Where(t => t.GetCustomAttribute<CommandHandlerAttribute>() is not null);
        
        foreach (var handler in handlers)
        {
            var type = handler.AsType();
            var obj = (ICommandHandler)_provider.GetRequiredService(type);
            var attr = type.GetCustomAttribute<CommandHandlerAttribute>()!;
            
            Handlers.Add(attr.Command, obj);
        }
    }

    public Task Handle(string action, Message message, CancellationToken token)
    {
        if (!Handlers.ContainsKey(action))
        {
            action = "help";
        }

        return Handlers[action].HandleMessage(message, token);
    }
}