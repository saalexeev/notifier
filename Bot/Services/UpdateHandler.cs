using Microsoft.Extensions.Logging;
using TanvirArjel.Extensions.Microsoft.DependencyInjection;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Polling;
using Telegram.Bot.Types;

namespace Bot.Services;

[ScopedService]
public class UpdateHandler : IUpdateHandler
{
    private readonly ILogger<UpdateHandler> _logger;
    private readonly CommandHandler _commandHandler;

    public UpdateHandler(ILogger<UpdateHandler> logger, CommandHandler commandHandler)
    {
        _logger = logger;
        _commandHandler = commandHandler;
    }

    public async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update,
        CancellationToken cancellationToken)
    {
        try
        {
            var handler = update switch
            {
                { Message: { } message } => MessageHandlerAsync(message, cancellationToken),
                { EditedMessage: { } message } => MessageHandlerAsync(message, cancellationToken),
                _ => UnknownUpdateHandler(update)
            };

            await handler;
        }
        catch (Exception ex)
        {
            _logger.LogError("Exception: {Exception}", ex);
            await botClient.SendTextMessageAsync(
                chatId: update.Message!.Chat.Id,
                text: "Я не могу обработать твой запрос, у меня лапки 🐾: " + ex.Message,
                cancellationToken: cancellationToken
            );
        }
    }


    private Task MessageHandlerAsync(Message message, CancellationToken token)
    {
        _logger.LogInformation("Receive message type: {type} from {chat}", message.Type, message.Chat.Id);
        if (message.Text is not { } messageText)
        {
            return Task.CompletedTask;
        }

        var actionName = messageText.Split(' ')[0].TrimStart('/');

        return _commandHandler.Handle(actionName, message, token);
    }

    private Task UnknownUpdateHandler(Update update)
    {
        _logger.LogWarning("Unknown update type: {UpdateType}", update.Type);
        return Task.CompletedTask;
    }

    public async Task HandlePollingErrorAsync(ITelegramBotClient botClient, Exception exception,
        CancellationToken cancellationToken)
    {
        var message = exception switch
        {
            ApiRequestException e => $"Telegram API error:\n[{e.ErrorCode}] {e.Message}",
            _ => exception.ToString()
        };

        _logger.LogError("Got error: {error}", message);

        if (exception is RequestException)
        {
            await Task.Delay(TimeSpan.FromSeconds(2), cancellationToken);
        }
    }
}