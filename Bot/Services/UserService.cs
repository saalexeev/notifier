using NotyService.Data.Repository;
using TanvirArjel.Extensions.Microsoft.DependencyInjection;
using Telegram.Bot.Types;
using User = NotyService.Data.Entity.User;

namespace Bot.Services;

[ScopedService]
public class UserService
{
    private readonly UsersRepository _usersRepository;

    public UserService(UsersRepository usersRepository)
    {
        _usersRepository = usersRepository;
    }

    public async Task<User> FetchUser(Chat chat, CancellationToken token = default)
    {
        var strChatId = chat.Id.ToString();

        var userResult = await _usersRepository.FindByTelegramIdAsync(strChatId);
        if (userResult.Value is not null) return userResult.Value;

        var user = new User
        {
            FirstName = chat.FirstName,
            TelegramId = chat.Id.ToString(),
            Username = chat.Username
        };
        var result = await _usersRepository.CreateAsync(user, token);
        if (result.IsFailure)
        {
            throw result.Exception;
        }

        return user;
    }

    public async Task MarkAuthorized(User user)
    {
        user.Authorized = true; 
        await _usersRepository.UpdateAsync(user);
    }
}