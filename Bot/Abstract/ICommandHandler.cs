using Telegram.Bot;
using Telegram.Bot.Types;

namespace Bot.Abstract;

public interface ICommandHandler
{
    public Task HandleMessage(Message message, CancellationToken token);
}