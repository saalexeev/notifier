using System.Reflection;
using Bot.Attributes;
using Bot.Services;
using Castle.Core.Internal;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using User = NotyService.Data.Entity.User;

namespace Bot.Abstract;

public abstract class CommandHandlerBase : ICommandHandler
{
    protected readonly ITelegramBotClient Client;
    private readonly UserService _userService;

    protected CommandHandlerBase(ITelegramBotClient client, UserService userService)
    {
        Client = client;
        _userService = userService;
    }

    public async Task HandleMessage(Message message, CancellationToken token)
    {
        await _userService.FetchUser(message.Chat, token);
        var attr = Assembly
            .GetAssembly(typeof(CommandHandlerBase))!
            .GetTypes()
            .Where(t => t.IsSubclassOf(typeof(CommandHandlerBase)))
            .Select(t => t.GetAttribute<CommandHandlerAttribute>())
            .First();

        var authRequired = attr.RequireAuth;
        if (authRequired && !await IsAuthorized(message, token))
        {
            await Unauthorized(message, token);
            return;
        }

        await Handle(message, token);
    }

    protected abstract Task Handle(Message message, CancellationToken token);
    
    protected static MessageEntity GetCommandFrom(MessageEntity[] entities)
    {
        if (entities is null)
        {
            throw new ArgumentNullException(nameof(entities));
        }

        foreach (var entity in entities)
        {
            if (entity.Type is MessageEntityType.BotCommand)
            {
                return entity;
            }
        }

        throw new InvalidOperationException("Command not found in message!");
    }

    protected async Task<bool> IsAuthorized(Message message, CancellationToken token = default)
    {
        var user = await _userService.FetchUser(message.Chat, token);
        return user.Authorized;
    }

    protected async Task Unauthorized(Message message, CancellationToken token = default)
    {
        var user = await _userService.FetchUser(message.Chat, token);
        await Unauthorized(user, token);
    }
    protected Task Unauthorized(User user, CancellationToken token = default)
    {
        return Client.SendTextMessageAsync(
            chatId: long.Parse(user.TelegramId),
            text: "Ты не авторизован",
            cancellationToken: token
        );
    }
}