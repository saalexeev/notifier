using System.ComponentModel.DataAnnotations;

namespace Bot.Attributes;

[AttributeUsage(AttributeTargets.Class)]
public class CommandHandlerAttribute : Attribute
{
    public string Command { get; set; }

    public string Description { get; set; }

    public string Usage { get; set; }

    public bool RequireAuth { get; set; }
}